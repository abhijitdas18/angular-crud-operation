import { Component, OnInit } from '@angular/core';
import { AirlinesService } from '../service/airlines.service';
import { FormControl, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { FlightModel } from '../model/FlightModel';
import { AirlineCodeNamePairs } from '../model/AirlineCodeNamePairs';
import { Location } from '@angular/common';
import { Router } from '@angular/router';


@Component({
  selector: 'app-modify-flight',
  templateUrl: './modify-flight.component.html',
  styleUrls: ['./modify-flight.component.css']
})
export class ModifyFlightComponent implements OnInit {

  createFlightForm: FormGroup;
  submitted = false;

  providerType: string;
  providerId: number;
  providerName: string;

  validPattern = "^[a-zA-Z0-9-]{3}$"; // alphanumeric exact 3 letters

  airlineForm = { provider_name: '', provider_code: '', provider_type: '' };

  errMessage: boolean = false;

  errText: string = '';

  isProviderCodeValid: boolean = true;

  isProviderTypeValid: boolean = false;

  isReadOnly: boolean = true;

  flightModels: FlightModel[];

  constructor(private airlinesService: AirlinesService,
    private formBuilder: FormBuilder,
    private codeNamePairs: AirlineCodeNamePairs,
    private location: Location,
    private router: Router) { }

  ngOnInit(): void {

    this.createFlightForm = this.formBuilder.group({
      provider_type: [null, Validators.required, Validators.pattern(this.validPattern)],
      provider_code: [null, Validators.required]
    });

    // this.form = new FormGroup({

    //   provider_code: new FormControl(this.airlineForm.provider_code, [
    //     Validators.required, Validators.pattern(this.validPattern)]),

    //   provider_type: new FormControl(this.airlineForm.provider_type, [
    //     Validators.required])
    // })

    // get the airlines data
    this.airlinesService.getAirLines()
      .subscribe(flightModel => this.flightModels = flightModel);
    console.log("Modify data retrive current data:");
    console.log(this.flightModels);

  }

  // validate the provider code from the airlines in-memory data store
  retrieveProviderTypeByCode(providerCode: string): string {

    console.log("Update :")
    console.log(this.flightModels);

    let providerType: string = '';
    this.errText = '';
    this.errMessage = false;

    // no data entered in provider code
    if (Object.keys(providerCode).length === 0) {
      console.log("providerCode is not entered. Return providerType :" + providerType);
      return providerType;
    } else {
      console.log("Valid Data is entered. Need to check the matched in database providerCode:" + providerCode);
      for (let airline of this.flightModels) {
        //console.log("type : " + airline.providerType);
        if (providerCode === airline.providerCode) {
          console.log("Correct Provider code entered.");
          providerType = airline.providerType;
          this.providerId = airline.id;
          this.providerName = airline.providerName;

          this.errMessage = false;
          //this.isProviderCodeValid = this.errMessage;
          this.isReadOnly = this.errMessage; // make the Provider Type enabled
          break;
        } else {
          console.log("Provider code not found.");
          providerType = ""
          this.errMessage = true;
          this.isReadOnly = this.errMessage; // make  Provider Type disabled
          //this.isProviderCodeValid = this.errMessage;
          this.errText = "Provider code not found.";
        }
      }
    }

    console.log("retrieveProviderTypeByCode :" + this.errMessage + " == " + this.errText);
    return providerType;
  }

  onBlur(providerCode: string): void {
    console.log('Focus Is Lost for this Element : ' + providerCode);
    this.providerType = this.retrieveProviderTypeByCode(providerCode);
    if(this.providerType !== '' || this.providerType !== "") {
      this.submitted = true;
    }else {
      this.submitted = false;
    }

    console.log("matched providerType :" + this.providerType);
    
    console.log("this.submitted :" + this.submitted);
  }

  goBack(): void {
    this.location.back();
  }

  update(providerCode: string, providerType: string): boolean {
    console.log("Update called : " + providerCode + "   " + providerType);
    this.isProviderTypeValid = false;
    if ("Domestic" === providerType || "International" === providerType) {
      let providerName: string = this.providerName;
      let id: number = this.providerId;
      this.airlinesService.updateAirLine({ id, providerName, providerCode, providerType } as FlightModel)
        .subscribe(() => this.router.navigate(['/']));
    } else {
      this.isProviderTypeValid = true;
    }
    return this.isProviderTypeValid;
  }
  // convenience getter for easy access to form fields
  get f() {
    return this.createFlightForm.controls;
  }

  get provider_code() { return this.createFlightForm.get('provider_code')!; }

  get provider_type() { return this.createFlightForm.get('provider_type')!; }


}
