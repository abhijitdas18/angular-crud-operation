import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import {CreateFlightComponent} from './create-flight/create-flight.component';
import {DeleteFlightComponent} from './delete-flight/delete-flight.component';
import {ModifyFlightComponent} from './modify-flight/modify-flight.component';
import {ViewFlightComponent} from './view-flight/view-flight.component';


const routes: Routes = [
  {
    path: 'create',
    component: CreateFlightComponent
  },
  {
    path: 'delete',
    component: DeleteFlightComponent
  },
  {
    path: 'modify',
    component: ModifyFlightComponent
  },
  {
    path: 'view',
    component: ViewFlightComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
