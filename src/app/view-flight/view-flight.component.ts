import { Component, OnInit } from '@angular/core';
import {AirlinesService} from '../service/airlines.service';

import { FlightModel } from '../model/FlightModel';

@Component({
  selector: 'app-view-flight',
  templateUrl: './view-flight.component.html',
  styleUrls: ['./view-flight.component.css']
})
export class ViewFlightComponent implements OnInit {

  flightModels : FlightModel[];

  text : string = '';

  searchTerm = '';


  constructor(private airlinesService : AirlinesService) { }

  ngOnInit() {
     this.getAirlines();
  }  

  getAirlines() : void {
    this.airlinesService.getAirLines()
    .subscribe(flightModel => this.flightModels = flightModel );
    console.log("View data :");
    console.log(this.flightModels);
  } 

  viewByFilter(x : any) {
    this.text += x.target.value + ' | ';
    console.log("Filter : " +  this.text);
  }

}
