import { Component, OnInit } from '@angular/core';
import { AirlinesService } from '../service/airlines.service';
import { FormControl, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { FlightModel } from '../model/FlightModel';
import { Location } from '@angular/common';
import { Router } from '@angular/router';

@Component({
  selector: 'app-create-flight',
  templateUrl: './create-flight.component.html',
  styleUrls: ['./create-flight.component.css']
})
export class CreateFlightComponent implements OnInit {

  providerTypeList = ['Domestic', 'International'];

  flightModels: FlightModel[];

  airlineForm = { provider_name: '', provider_code: '', provider_type: '' };

  createFlightForm: FormGroup;

  submitted = false;

  providerCode: string;

  isProviderCodeEntered: boolean = false;

  public myFlag: boolean = false;

  errorMsgProviderCode: string = "";

  codeNameMap = new Map([
    ["6E-", "INDIGO"],
    ["SG-", "SPICEJET"],
    ["I5-", "AIR ASIA"],
    ["G8-", "GO AIR"],
    ["9W-", "JET AIRWAYS"],
    ["AI-", "AIR INDIA"]
  ]);

  constructor(
    private airlinesService: AirlinesService,
    private formBuilder: FormBuilder,
    private location: Location,
    private router: Router) {

  }

  ngOnInit(): void {

    this.getAirlines();
    

    // this.createFlightForm = new FormGroup({
    //   provider_name: new FormControl(this.airlineForm.provider_name, [
    //     Validators.required,
    //     Validators.maxLength(10),
    //     Validators.pattern('^[a-zA-Z ]*$'),
    //     Validators.nullValidator]),

    //   provider_code: new FormControl(this.airlineForm.provider_code, [
    //     Validators.required]),

    //   provider_type: new FormControl(this.airlineForm.provider_type, [
    //     Validators.required])
    // });

    this.createFlightForm = this.formBuilder.group({
      provider_name: [null, 
        Validators.required, 
        Validators.maxLength(11), 
        Validators.pattern('^[a-zA-Z ]*$'),
        Validators.nullValidator],

      provider_code: [null, 
        Validators.required],

      provider_type: [null, 
        Validators.required]

    });


  }

  getAirlines(): void {
    this.airlinesService.getAirLines()
      .subscribe(flightModel => this.flightModels = flightModel);
  }


  add(providerName: string, providerType: string): void {
    // console.log("Values: "  + this.airlineForm.provider_name + " " +this.airlineForm.provider_code + " "+ this.airlineForm.provider_type);
    let  providerCode : string = this.providerCode;
    console.log(providerName + "  providerCode:  " + providerCode + " providerType : " + providerType);
    this.airlinesService.addAirLine({ providerName, providerCode, providerType } as FlightModel)
      .subscribe(data => {
        console.log("Final created data :")
        console.log(data);
        this.flightModels.push(data);
        console.log(this.flightModels);
        this.router.navigate(['/']);
      },
        err => {
          alert("something went wrong.");
        })
  }

  goBack(): void {
    this.location.back();
  }

  get provider_name() { return this.createFlightForm.get('provider_name')!; }

  get provider_type() { return this.createFlightForm.get('provider_type')!; }

  get provider_code() { return this.createFlightForm.get('provider_code')!; }

  onBlur(providerName: string): void {
    this.myFlag = false;
    console.log('Focus Is Lost for this Element : ' + providerName);
    this.providerCode = this.retrieveProviderCodeByName(providerName);
    console.log('this.providerCode  : ' + this.providerCode);

    if (this.providerCode == " ") {
      //this.myFlag = !this.myFlag;
      this.myFlag = true;
    } else {
      this.submitted = true;
      this.isProviderCodeEntered = true;
    }

    if (this.myFlag) {
      this.errorMsgProviderCode = "Provider Code is required or not matched.";
    } else {
      this.errorMsgProviderCode = " ";
    }


    console.log("matched providerCode :" + this.providerCode);
    console.log("this.myFlag :" + this.myFlag);
    console.log("errorMsgProviderCode : " + this.errorMsgProviderCode);
  }

  // Get the provider code based on the provider name
  retrieveProviderCodeByName(providerName: string): string {

    let providerCode: string = '';
    for (let entry of this.codeNameMap.entries()) {
      if (providerName === entry[1]) {
        providerCode = entry[0];
        break;
      } else {
        providerCode = " ";
      }
    }
    return providerCode;
  }
}
