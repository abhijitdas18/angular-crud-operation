import { Injectable } from '@angular/core';


@Injectable({
  providedIn: 'root'
})

  export class AirlineCodeNamePairs {

    constructor(){
        console.log("AirlineCodeNamePairs");
    }



    map = new Map([
        ["6E-", "INDIGO"],
        ["SG-", "SPICEJET"],
        ["I5-", "AIR ASIA"],
        ["G8-", "GO AIR"],
        ["9W-", "JET AIRWAYS"],
        ["AI-", "AIR INDIA"]
    ]);
  }  