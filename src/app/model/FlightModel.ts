export class FlightModel {
  id: number;
  providerName: string;
  providerCode: string; 
  providerType: string;    
}