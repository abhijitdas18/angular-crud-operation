export interface AirlineModel {
    id: number;
    providerName: string;
    providerCode: string; 
    providerType: string;
}