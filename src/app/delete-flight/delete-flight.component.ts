import { Component, OnInit } from '@angular/core';
import { AirlinesService } from '../service/airlines.service';
import { FormBuilder, FormGroup, Validators ,FormControl} from '@angular/forms';
import { FlightModel } from '../model/FlightModel';
import { AirlineCodeNamePairs } from '../model/AirlineCodeNamePairs';
import { Location } from '@angular/common';


@Component({
  selector: 'app-delete-flight',
  templateUrl: './delete-flight.component.html',
  styleUrls: ['./delete-flight.component.css']
})
export class DeleteFlightComponent implements OnInit {

  createFlightForm: FormGroup;
  submitted = false;

  providerType: string;
  providerId: number;
  providerName: string;
  providerCode: string;

  providerCodeExist: boolean = false;

  isReadOnly: boolean = true;

  flightModels: FlightModel[];

  errMessage : boolean = false;

  validPattern = "^[a-zA-Z0-9-]{3}$";
  
  validAlpha = "^[a-zA-Z]$"; //"/^[a-zA-Z]*$/"  // Alphabets no blankspace:

  constructor(private airlinesService: AirlinesService,
    private formBuilder: FormBuilder,
    private codeNamePairs: AirlineCodeNamePairs,
    private location: Location) { }

  ngOnInit(): void {

    this.createFlightForm = this.formBuilder.group({
      provider_code: [null, Validators.required, Validators.pattern(this.validPattern), Validators.nullValidator],
      provider_type: [null, Validators.required, Validators.nullValidator, Validators.maxLength(13), Validators.pattern(this.validAlpha)] 
    });

    // this.form = new FormGroup({
    // provider_code: new FormControl('', [
    //   Validators.required, Validators.pattern(this.validPattern), Validators.nullValidator]),

    // provider_type: new FormControl('', [
    //   Validators.required, Validators.pattern('^[a-zA-Z ]*$'), Validators.maxLength(13),  Validators.nullValidator])
    // });


    // get the airlines data
    this.airlinesService.getAirLines()
      .subscribe(flightModel => this.flightModels = flightModel);
    console.log("Modify data retrive current data:");
    console.log(this.flightModels);

  }

  // check the provider code is found in  in-memory airlines
  validProviderCode(providerCode: string): boolean {
    let providerCodeExist: boolean = false;
    for (let airline of this.flightModels) {
      if (providerCode === airline.providerCode) {
        providerCodeExist = true;
        this.providerId = airline.id;
        this.providerCode = airline.providerCode;
        this.providerType = airline.providerType;
        break;
      }
    }
    return providerCodeExist;
  }

  onBlur(providerCode: string): void {
    console.log('Focus Is Lost for this Element : ' + providerCode);
    this.isReadOnly = !this.validProviderCode(providerCode);
    this.submitted = true;
    console.log("validProviderCode :" + this.validProviderCode(providerCode));
    console.log("isReadOnly :" + this.isReadOnly);
  }

  goBack(): void {
    this.location.back();
  }


  get provider_code() { return this.createFlightForm.get('provider_code')!; }

  get provider_type() { return this.createFlightForm.get('provider_type')!; }


  delete(providerCode: string, providerType: string): string {

    this.errMessage = false;
    console.log("Delete called." + providerCode + " " + providerType );
    console.log(providerCode === this.providerCode);
    console.log(providerType === this.providerType);
    console.log(providerCode === this.providerCode && providerType === this.providerType);
    if (providerCode === this.providerCode && providerType === this.providerType) {
      console.log("matched");
      this.airlinesService.deleteAirLine(this.providerId)
        .subscribe(() => this.goBack());
        return "Successfully deleted";
    }else{
      this.errMessage = true;
      console.log(" Not matched");
    }
    return "Data is not found"
  }
  // convenience getter for easy access to form fields
  get f() {
    return this.createFlightForm.controls;
  }


}
