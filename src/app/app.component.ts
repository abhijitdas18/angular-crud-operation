import { Component } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title:string = 'my first app';

  constructor(private _router: Router) { }

  navigateToCreate() {
    //alert("create called");
    this._router.navigateByUrl('/create')
  }

  navigateToDelete() {
   // alert("delete called");
    this._router.navigateByUrl('/delete')
  }

  navigateToModify() {
    //alert("modify called");
    this._router.navigateByUrl('/modify')
  }

  navigateToView() {
   // alert("view called");
    this._router.navigateByUrl('/view')
  } 
}
