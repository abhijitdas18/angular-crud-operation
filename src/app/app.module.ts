import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { CreateFlightComponent } from './create-flight/create-flight.component';
import { ModifyFlightComponent } from './modify-flight/modify-flight.component';
import { DeleteFlightComponent } from './delete-flight/delete-flight.component';
import { ViewFlightComponent } from './view-flight/view-flight.component';
import { FormsModule } from '@angular/forms';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { InMemoryWebApiModule } from 'angular-in-memory-web-api';  
import { HttpClientInMemoryWebApiModule } from 'angular-in-memory-web-api'; //
import { InMemomoryAirlinesDataService } from './service/in-memory-airlines-data.service'; //
import { HttpClientModule } from '@angular/common/http'; //
import {  ReactiveFormsModule } from '@angular/forms';
import { FilterPipe } from './service/filter.pipe';

@NgModule({
  declarations: [
    AppComponent,
    CreateFlightComponent,
    ModifyFlightComponent,
    DeleteFlightComponent,
    ViewFlightComponent,
    FilterPipe
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    NgbModule,
    HttpClientModule, //
    // The HttpClientInMemoryWebApiModule module intercepts HTTP requests
    // and returns simulated server responses.
    // Remove it when a real server is ready to receive requests.
    HttpClientInMemoryWebApiModule.forRoot(
      InMemomoryAirlinesDataService, { dataEncapsulation: false }
)
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
