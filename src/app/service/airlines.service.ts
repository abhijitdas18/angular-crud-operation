import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable, of } from 'rxjs';
import { catchError, map, tap } from 'rxjs/operators';
import { MessageService } from '../service/message.service';
import { FlightModel } from '../model/FlightModel';


@Injectable({
  providedIn: 'root'
})
export class AirlinesService {

  private airlinesUrl = 'api/airLines';  // URL to web api

  httpOptions = {
    headers: new HttpHeaders({ 'Content-Type': 'application/json' })
  };

  constructor(private httpClient: HttpClient,
    private messageService: MessageService) { }

  getAirLines(): Observable<FlightModel[]> {
    return this.httpClient.get<FlightModel[]>(this.airlinesUrl)
      .pipe(
        tap(_ => this.log('fetched Airliens')),
        catchError(this.handleError<FlightModel[]>('getAirLines', []))
      );
  }

  //////// Save methods //////////
  /** POST: add a new Airline to the server */
  addAirLine(flightModel: FlightModel): Observable<FlightModel> {
    console.log("Add service  called : " + flightModel);
    console.log(flightModel);
    return this.httpClient.post<FlightModel>(this.airlinesUrl, flightModel, this.httpOptions).pipe(
      tap((newAirLine: FlightModel) => this.log(`added AirLine w/ id=${newAirLine.id}`)),
      catchError(this.handleError<FlightModel>('addAirLine'))
    );
  }

  /** DELETE: delete the hero from the server */
  deleteAirLine(id : number): Observable<FlightModel> {
    //const id = typeof flightModel === 'number' ? flightModel : flightModel.id;
    const url = `${this.airlinesUrl}/${id}`;

    return this.httpClient.delete<FlightModel>(url, this.httpOptions).pipe(
      tap(_ => this.log(`deleted AirLine id=${id}`)),
      catchError(this.handleError<FlightModel>('deleteAirLine'))
    );
  }


/** PUT: update the hero on the server */
updateAirLine(flightModel: FlightModel): Observable<any> {
  console.log("Update service  called : " + flightModel);
  console.log(flightModel);
  return this.httpClient.put(this.airlinesUrl, flightModel, this.httpOptions).pipe(
    tap(_ => this.log(`updated flightModel id=${flightModel.id}`)),
    catchError(this.handleError<any>('updateAirLine'))
  );
}

  /**
 * Handle Http operation that failed.
 * Let the app continue.
 * @param operation - name of the operation that failed
 * @param result - optional value to return as the observable result
 */
  private handleError<T>(operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {

      // TODO: send the error to remote logging infrastructure
      console.error(error); // log to console instead

      // TODO: better job of transforming error for user consumption
      this.log(`${operation} failed: ${error.message}`);

      // Let the app keep running by returning an empty result.
      return of(result as T);
    };
  }

  /** Log a AirlineService message with the MessageService */
  private log(message: string) {
    this.messageService.add(`AirLineService: ${message}`);
  }
}
