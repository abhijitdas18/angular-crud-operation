import { Injectable } from '@angular/core';
import { InMemoryDbService } from 'angular-in-memory-web-api'
import { FlightModel } from '../model/FlightModel';
@Injectable({
  providedIn: 'root'
})
export class InMemomoryAirlinesDataService  implements InMemoryDbService {

  constructor() { }


  createDb() {
    let airLines = [
      {
        id: 1, 
        providerName: 'Jet Airways',
        providerCode: '9W-', 
        providerType: 'Domestic'
      },
      {
        id: 2, 
        providerName: 'Emirates',
        providerCode: 'EK-', 
        providerType: 'International'
      }
    ];
    return { airLines };
  }

  // Overrides the genId method to ensure that a hero always has an id.
  // If the heroes array is empty,
  // the method below returns the initial number (11).
  // if the heroes array is not empty, the method below returns the highest
  // hero id + 1.
  genId(airLines: FlightModel[]): number {
    console.log("genId() :airLines.length : " + airLines.length);
    let genId : number = airLines.length > 0 ? Math.max(...airLines.map(airline => airline.id)) + 1 : 11;
    console.log("genId() :genId : " + genId);
    return genId;
  }
  
}
