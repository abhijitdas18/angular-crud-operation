import { Pipe, PipeTransform } from '@angular/core';

@Pipe({ name: 'searchFilter' })
export class FilterPipe implements PipeTransform {
  /**
   * Pipe filters the list of elements based on the search text provided
   *
   * @param items list of elements to search in
   * @param searchText search string
   * @returns list of elements filtered by search text or []
   */
  transform(list: any[], filterText: string): any {
    return list ? list.filter(item => item.providerType.search(new RegExp(filterText, 'i')) > -1) : [];
  }
}